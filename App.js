import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Dimensions,
  View,
  Text,
  TouchableHighlight,
  Image,
  NativeAppEventEmitter,
  NativeEventEmitter,
  NativeModules,
  Platform,
  PermissionsAndroid,
  AppState,
} from 'react-native';
import BleManager from 'react-native-ble-manager';
import firebaseService from './library/services/FirebaseService';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      scanning: false,
      peripherals: new Map(),
      appState: '',
      sensorData: {
        device: '',
        bpm: 0,
        rawData: 0
      }
    };
    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(
        this,
    );
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(
        this,
    );
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    BleManager.start({showAlert: false});
    this.handlerDiscover = bleManagerEmitter.addListener(
        'BleManagerDiscoverPeripheral',
        this.handleDiscoverPeripheral,
    );
    this.handlerStop = bleManagerEmitter.addListener(
        'BleManagerStopScan',
        this.handleStopScan,
    );
    this.handlerDisconnect = bleManagerEmitter.addListener(
        'BleManagerDisconnectPeripheral',
        this.handleDisconnectedPeripheral,
    );
    this.handlerUpdate = bleManagerEmitter.addListener(
        'BleManagerDidUpdateValueForCharacteristic',
        this.handleUpdateValueForCharacteristic,
    );
    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(
          PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
      ).then(result => {
        if (result) {
          console.log('Permission is OK');
        } else {
          PermissionsAndroid.requestPermission(
              PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
          ).then(result => {
            if (result) {
              console.log('User accept');
            } else {
              console.log('User refuse');
            }
          });
        }
      });
    }
  }

  handleAppStateChange(nextAppState) {
    if (
        this.state.appState.match(/inactive|background/) &&
        nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
      BleManager.getConnectedPeripherals([]).then(peripheralsArray => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    this.setState({appState: nextAppState});
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
    this.handlerUpdate.remove();
  }

  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    console.log(data.value);
    let sensors = this.state.sensorData;
    let dateTime = Date.now();
    let sensor = {
      bpm: data.value[1],
      rawData: data[4]
    };
    sensors.push(sensor);
    // send data to cloud

    this.setState(sensors);
  }

  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({scanning: false});
  }

  startScan() {
    if (!this.state.scanning) {
      //this.setState({peripherals: new Map()});
      BleManager.scan([], 3, true).then(results => {
        console.log('Scanning...');
        this.setState({scanning: true});
      });
    }
  }

  retrieveConnected() {
    BleManager.getConnectedPeripherals([]).then(results => {
      if (results.length == 0) {
        console.log('No connected peripherals');
      }
      var peripherals = this.state.peripherals;
      for (var i = 0; i < results.length; i++) {
        var peripheral = results[i];
        peripheral.connected = true;
        peripherals.set(peripheral.id, peripheral);
        this.setState({ peripherals });
      }
    });
  }

  handleDiscoverPeripheral(peripheral) {
    var peripherals = this.state.peripherals;
    console.log('Got ble peripheral', peripheral);
    if (!peripheral.name) {
      peripheral.name = 'NO NAME';
    }else {
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
  }

  getData(peripheral) {
    if (peripheral) {
      if (peripheral.connected) {
        BleManager.disconnect(peripheral.id);
      } else {
        BleManager.connect(peripheral.id)
            .then(() => {
              let peripherals = this.state.peripherals;
              let p = peripherals.get(peripheral.id);
              if (p) {
                p.connected = true;
                peripherals.set(peripheral.id, p);
                this.setState({ peripherals });
              }
              console.log('Connected to ' + peripheral.id);

              setTimeout(() => {
                BleManager.retrieveServices(peripheral.id).then(
                    peripheralInfo => {
                      console.log(peripheralInfo);
                      var service = '180d';
                      var bakeCharacteristic = '2a37';
                      var crustCharacteristic = '2901';

                      setTimeout(() => {
                        BleManager.startNotification(
                            peripheral.id,
                            service,
                            bakeCharacteristic,
                        )
                            .then(() => {
                              console.log('Started notification on ' + peripheral.id);
                              setTimeout(() => {
                                BleManager.write(
                                    peripheral.id,
                                    service,
                                    crustCharacteristic,
                                    [0],
                                ).then(() => {
                                  console.log('Writed Command for device');
                                  BleManager.write(
                                      peripheral.id,
                                      service,
                                      bakeCharacteristic,
                                      [1, 95],
                                  ).then(() => {
                                    console.log('Device Respond');
                                  });
                                });
                              }, 500);
                            })
                            .catch(error => {
                              console.log('Notification error', error);
                            });
                      }, 200);
                    },
                );
              }, 1);
            })
            .catch(error => {
              console.log('Connection error', error);
            });
      }
    }
  }

  renderItem(item) {
    const color = item.connected ? 'green' : '#fff';
    return (
        <TouchableHighlight onPress={() => this.getData(item)}>
          <View style={[styles.row, {backgroundColor: color}]}>
            <Text
                style={{
                  fontSize: 12,
                  textAlign: 'center',
                  color: '#333333',
                  padding: 10,
                }}>
              {item.name}
            </Text>
            <Text
                style={{
                  fontSize: 10,
                  textAlign: 'center',
                  color: '#333333',
                  padding: 2,
                }}>
              RSSI: {item.rssi}
            </Text>
            <Text
                style={{
                  fontSize: 8,
                  textAlign: 'center',
                  color: '#333333',
                  padding: 2,
                  paddingBottom: 20,
                }}>
              {item.id}
            </Text>
          </View>
        </TouchableHighlight>
    );
  }

  renderLog(item) {
    return (
        <View style={[styles.row, {backgroundColor: '#F9F9F9'}]}>
          <Text
              style={{
                fontSize: 12,
                textAlign: 'center',
                color: 'red',
                padding: 10,
              }}>
            {item.message}
          </Text>
        </View>
    );
  }

  render() {
    let { bpm, rawData } = this.state.sensorData;
    return (
      <SafeAreaView style={styles.container}>
        <TouchableHighlight style={styles.header} onPress={this.startScan()}>
          <Text>Scan Devices ({this.state.scanning ? 'on' : 'off'})</Text>
        </TouchableHighlight>
        <TouchableHighlight
            style={{
              marginTop: 0,
              margin: 20,
              padding: 5,
              width: '100%',
              backgroundColor: '#ccc',
            }}
            onPress={() => this.retrieveConnected()}>
          <Text>List of Devices</Text>
        </TouchableHighlight>
        <View style={styles.circular}>
          <Text style={styles.textValue}>{bpm}</Text>
        </View>
        <View style={styles.rect}>
          <Text style={styles.textValue}>BPM</Text>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#252525',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
  },
  header: {
    borderRadius: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    width: '100%',
    marginBottom: 10,
  },
  circular: {
    width: 220,
    height: 220,
    borderRadius: 110,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textValue: {
    fontSize: 100,
    fontWeight: 'bold',
    color: 'white',
  },
  scroll: {
    flex: 1,
    backgroundColor: '#f0f0f0',
    margin: 10,
  },
  row: {
    margin: 10,
  }
});

export default App;
