import * as firebaseService from 'firebase';

const settings = {timestampsInSnapshots: true};

const config = {
    apiKey: "AIzaSyCn4TaRqvmtwrKbuD8tqrcOeCXndNpL0o0",
    authDomain: "iot-pulse-4b336.firebaseapp.com",
    databaseURL: "https://iot-pulse-4b336.firebaseio.com",
    projectId: "iot-pulse-4b336",
    storageBucket: "iot-pulse-4b336.appspot.com",
    messagingSenderId: "879200618660",
    appId: "1:879200618660:web:2fd4bfe3f22da2e65b7e31",
    measurementId: "G-BBY55GY10L"
};
firebaseService.initializeApp(config);
export default firebaseService;
